<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Login                Please login to ma_5151a8</name>
   <tag></tag>
   <elementGuidId>cd3b714b-3542-4f99-8b58-b0a7be955bd2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.row</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//section[@id='login']/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>723555a0-0a29-443d-a97d-5f0b9f06210f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>row</value>
      <webElementGuid>b1393300-3831-4f0c-b569-eaa67e78c935</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            
                Login
                Please login to make appointment.
                            
            
                
                    
                        
                            Demo account
                            
                                
                                  
                                  
                                
                            
                        
                        
                            
                                
                                  
                                  
                                
                            
                        
                    
                    
                        Username
                        
                            
                        
                    
                    
                        Password
                        
                            
                        
                    
                    
                        
                            Login
                        
                    
                
            
        </value>
      <webElementGuid>66aaeb0d-ebe5-4cbe-abb2-faf85821437c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;login&quot;)/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]</value>
      <webElementGuid>61211867-2ea7-4ca2-b61c-6bfa43b6563d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='login']/div/div</value>
      <webElementGuid>bba9cdbe-766a-452f-994c-9c45a360ef99</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Make Appointment'])[1]/following::div[2]</value>
      <webElementGuid>0d7bc06b-13e6-4a27-8cac-c34a4d018a68</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='We Care About Your Health'])[1]/following::div[2]</value>
      <webElementGuid>ea668141-bec0-4974-a081-5d9c35638fbd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div</value>
      <webElementGuid>5132d7f2-9611-406b-b8c8-c92d6118ae63</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
            
                Login
                Please login to make appointment.
                            
            
                
                    
                        
                            Demo account
                            
                                
                                  
                                  
                                
                            
                        
                        
                            
                                
                                  
                                  
                                
                            
                        
                    
                    
                        Username
                        
                            
                        
                    
                    
                        Password
                        
                            
                        
                    
                    
                        
                            Login
                        
                    
                
            
        ' or . = '
            
                Login
                Please login to make appointment.
                            
            
                
                    
                        
                            Demo account
                            
                                
                                  
                                  
                                
                            
                        
                        
                            
                                
                                  
                                  
                                
                            
                        
                    
                    
                        Username
                        
                            
                        
                    
                    
                        Password
                        
                            
                        
                    
                    
                        
                            Login
                        
                    
                
            
        ')]</value>
      <webElementGuid>58b9bddd-6ae3-4c7c-ba20-16b287ad925f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
